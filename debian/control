Source: ruby-hmac
Section: ruby
Priority: optional
Maintainer: Daigo Moriwaki <daigo@debian.org>
Uploaders: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>,
           Paul van Tilburg <paulvt@debian.org>
Build-Depends: debhelper (>= 9),
               gem2deb,
               ruby-minitest
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/ruby-hmac.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/ruby-hmac.git
Homepage: http://ruby-hmac.rubyforge.org
XS-Ruby-Versions: all
Testsuite: autopkgtest-pkg-ruby

Package: ruby-hmac
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Ruby interface for HMAC algorithm
 This module provides common interface to HMAC functionality. HMAC is a
 kind of "Message Authentication Code" (MAC) algorithm whose standard is
 documented in RFC2104. Namely, a MAC provides a way to check the integrity
 of information transmitted over or stored in an unreliable medium, based
 on a secret key.
